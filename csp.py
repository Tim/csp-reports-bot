#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: ts=4 sts=4 sw=4 et
#
# Slixmpp: The Slick XMPP Library
# Copyright (C) 2010  Nathanael C. Fritz
#
# Copyright (C) 2017  Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>
# Copyright (C) 2017  Maxime “pep” Buquet <pep@collabora.com>
# This file is licensed under the MIT license, see the LICENSE file.


import json
import logging
from getpass import getpass
from argparse import ArgumentParser
from pprint import pformat
from xml.etree import cElementTree as ET

import asyncio
from slixmpp.clientxmpp import ClientXMPP
from slixmpp import Message
from aiohttp import web

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter

LEXER = get_lexer_by_name('json')
FORMATTER = HtmlFormatter(noclasses=True)

log = logging.getLogger(__name__)


class Frontend(web.Application):
    def __init__(self, xmpp):
        super().__init__()
        self.xmpp = xmpp
        self.router.add_route('GET', '/report-csp-violation', self.handle_get)
        self.router.add_route(
            'POST', '/report-csp-violation', self.handle_csp_violation,
        )

    def handle_get(self, request):
        return web.Response(text='Hello world.')

    async def handle_csp_violation(self, request):
        if request.content_type != 'application/csp-report':
            return web.Response(text='Hehe. :p')
        csp_report = await request.json()
        real_ip = request.headers.get('X-REAL-IP')
        self.xmpp.post_violation(
            csp_report, real_ip, request.headers['USER-AGENT'],
        )
        return web.Response(text='Thanks for the report.')


class Bot(ClientXMPP):

    """
    A simple Slixmpp client to send messages.
    """

    def __init__(self, jid, password):
        ClientXMPP.__init__(self, jid, password)

        self.register_plugin('xep_0030')
        self.register_plugin('xep_0050')
        self.register_plugin('xep_0071')
        self.register_plugin('xep_0092', {
            'software_name': 'CSP-report',
            'version': '0.0.1',
            'os': 'Linux',
        })  # Software Version
        self.register_plugin('xep_0199')

    def post_violation(self, report, ip, ua):
        subject = 'CSP Violation Report from %s' % ip,
        body = ('From %s (%s):\n' % (ip, ua)) + pformat(report)
        html_body = ('From %s (%s):' % (ip, ua)) + \
            (highlight(json.dumps(report), LEXER, FORMATTER)
                .strip().replace('\n</pre>', '</pre>'))
        message = self.send_message(
            mto='maxime.buquet@collabora.co.uk',
            msubject=subject, mbody=body, mhtml=html_body,
        )


if __name__ == '__main__':
    parser = ArgumentParser(description=Bot.__doc__)

    parser.add_argument(
        '-q', '--quiet', help='set logging to ERROR', action='store_const',
        dest='loglevel', const=logging.ERROR, default=logging.INFO,
    )
    parser.add_argument(
        '-d', '--debug', help='set logging to DEBUG', action='store_const',
        dest='loglevel', const=logging.DEBUG, default=logging.INFO,
    )
    parser.add_argument(
        '-j', '--jid', dest='jid', help='JID to use',
    )
    parser.add_argument(
        '-p', '--password', dest='password', help='password to use',
    )

    args = parser.parse_args()

    if args.jid is None:
        args.jid = input('Client JID: ')
    if args.password is None:
        args.password = getpass('Password: ')

    logging.basicConfig(level=args.loglevel,
                        format='%(asctime)s %(levelname)-8s %(message)s')

    xmpp = Bot(args.jid, args.password)

    app = Frontend(xmpp)
    handler = app.make_handler()
    http = app.loop.run_until_complete(
        app.loop.create_server(handler, '::1', 8089),
    )

    xmpp.connect()
    try:
        xmpp.process()
    except KeyboardInterrupt:
        xmpp.do_disconnect()
        xmpp.process(forever=False)
